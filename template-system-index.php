<?php
/*
Template Name: Systeem index
*/

    if (!defined('ABSPATH')) {
        die();
    }

    global $avia_config, $wp_query;

    /*
     * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
     */
    get_header();

    /**
     * @used_by				enfold\config-wpml\config.php				10
     * @since 4.5.1
     */
    do_action('ava_page_template_after_header');

     if (get_post_meta(get_the_ID(), 'header', true) != 'no') {
         echo avia_title();
     }

     do_action('ava_after_main_title');
     ?>

		<div class='container_wrap container_wrap_first main_color rffw <?php avia_layout_class('main'); ?>'>

			<div class='container'>

				<main class='template-page content  <?php avia_layout_class('content'); ?> units' <?php avia_markup_helper(array('context' => 'content','post_type'=>'page'));?>>

                    <?php
                    /* Run the loop to output the posts.
                    * If you want to overload this in a child theme then include a file
                    * called loop-page.php and that will be used instead.
                    */

                    $avia_config['size'] = avia_layout_class('main', false) == 'fullsize' ? 'entry_without_sidebar' : 'entry_with_sidebar';
                    get_template_part('includes/loop', 'page');
                    ?>

				<!--end content-->

        <div class="post-entry post-entry-type-page">
          <div class="entry-content-wrapper clearfix">

            <div style="padding-bottom:40px; margin-top:40px; " class="av-special-heading av-special-heading-h1  blockquote modern-quote  avia-builder-el-1  el_before_av_one_third  avia-builder-el-first   ">
              <h1 class="av-special-heading-tag " itemprop="headline"><?php echo get_the_title(); ?></h1>
              <div class="special-heading-border">
                <div class="special-heading-inner-border">
                </div>
              </div>
            </div>

        <div class="flex_column_table av-equal-height-column-flextable -flextable">

          <?php

          $vendors = new WP_Query( array(
            'post_type' => 'vendor',
            'post_status' => 'publish',
            'posts_per_page' => -1
          ) );


          if ( $vendors->have_posts() ) {

            $i = 0;
          	// The Loop
          	while ( $vendors->have_posts() ) {
              $vendors->the_post();
              
              if(get_post_meta( get_the_ID(), 'show_on_website', true ) == '1'){
                ?>

                <div class="flex_column av_one_third  av-animated-generic fade-in  flex_column_table_cell av-equal-height-column av-align-top avia-link-column av-column-link first  avia-builder-el-2  el_after_av_heading  el_before_av_one_third  card-1 erp-tile  avia_start_animation avia_start_delayed_animation" style="background: #ffffff; padding:30px; background-color:#ffffff; border-radius:0px; " data-link-column-url="<?php echo the_permalink(); ?>">

                  <a class="av-screen-reader-only" href="<?php echo the_permalink(); ?>">Link to: <?php echo get_post_meta( get_the_ID(), 'system_name', true ); ?></a>
                  <div style="padding-bottom:0px; /*margin-top:30px;*/ " class="av-special-heading av-special-heading-h3  blockquote modern-quote  avia-builder-el-3  el_before_av_textblock  avia-builder-el-first  fw-800 p-t-20 ">
                    <h3 class="av-special-heading-tag " itemprop="headline"><?php echo get_post_meta( get_the_ID(), 'system_name', true ); ?></h3>
                    <div class="special-heading-border">
                      <div class="special-heading-inner-border"></div>
                    </div>
                  </div>

                  <section class="av_textblock_section " itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                    <div class="avia_textblock  " style="font-size:15px; " itemprop="text">
                      <p>
                        Leverancier: <?php echo get_the_title(); ?></br>
                        <!-- vendor version: <?php echo get_post_meta( get_the_ID(), 'vendor_version', true ); ?> -->
                      </p>
                    </div>
                  </section>

                  <div class="avia-image-container  av-styling- avia-builder-el-5  el_after_av_textblock  el_before_av_textblock   avia-align-left " itemprop="ImageObject" itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                    <div class="avia-image-container-inner">
                      <div class="avia-image-overlay-wrap">

                        <?php
                        //echo "<pre>" . print_r(get_post_meta( get_the_ID()), true) . "</pre>";

                        echo wp_get_attachment_image(get_post_meta( get_the_ID(), 'system_logo_id', true ), 'medium_large');

                        //<img class="avia_image" src="https://staging-erpportal.kinsta.cloud/wp-content/uploads/2018/11/Epicor-High-Resolution-Logo-300x105.jpg" alt="" title="Epicor-High-Resolution-Logo" height="105" width="300" itemprop="thumbnailUrl">
                        ?>

                      </div>
                    </div>
                  </div>

                  <!-- <section class="av_textblock_section " itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                    <div class="avia_textblock  " style="font-size:15px; " itemprop="text">
                      <?php echo get_post_meta( get_the_ID(), 'vendor_information', true ); ?>
                    </div>
                  </section> -->
                </div>

                <div class="av-flex-placeholder"></div>

                <?php
                $i++;
                if ($i % 3 == 0) {echo '</div><div style="margin-top:60px;" class="flex_column_table av-equal-height-column-flextable -flextable">';}
              }
            }
          	wp_reset_postdata();
          }
          ?>




      </div><!--close column table wrapper. Autoclose: 1 -->
    </div>
  </div>

				</main>

				<?php

                //get the sidebar
                $avia_config['currently_viewing'] = 'page';
                get_sidebar();

                ?>

			</div><!--end container-->

		</div><!-- close default .container_wrap element -->



<?php get_footer(); ?>
