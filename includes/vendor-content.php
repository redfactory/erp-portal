<?php echo  do_shortcode("
              <!-- wp:shortcode -->
              [av_submenu which_menu='custom' menu='3' position='left' color='main_color' sticky='aviaTBsticky' mobile='disabled' av_uid='av-18rv6q' custom_class='']
                  [av_submenu_item title='Bedrijfs informatie' link='manually, #Bedrijfsinformatie' linktarget='' button_style='av-menu-button av-menu-button-bordered' av_uid='av-78kky']
                  [av_submenu_item title='Product Info' link='manually, #ProductInfo' linktarget='' button_style='' av_uid='av-oonoy']
                  [av_submenu_item title='Product screenshots' link='manually,#ProductInfo' linktarget='' button_style='' av_uid='av-j7vl6']
                  [av_submenu_item title='Nieuws' link='manually,#ProductInfo' linktarget='' button_style='' av_uid='av-1ayrm']
                  [av_submenu_item title='Contact' link='manually,#Contact' linktarget='' button_style='' av_uid='av-aakgi']
              [/av_submenu]

              [av_one_half first min_height='' vertical_alignment='' space='' custom_margin='' margin='0px' row_boxshadow='' row_boxshadow_color='' row_boxshadow_width='10' link='' linktarget='' link_hover='' padding='0px' highlight='' highlight_size='' border='' border_color='' radius='0px' column_boxshadow='' column_boxshadow_color='' column_boxshadow_width='10' background='bg_color' background_color='' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='' background_position='top left' background_repeat='no-repeat' animation='' mobile_breaking='' mobile_display='' av_uid='av-20lu6a']
                  [av_heading heading='Epicor ERP 10' tag='h1' link_apply='' link='manually,http://' link_target='' style='blockquote modern-quote' size='' subheading_active='' subheading_size='15' margin='' padding='10' color='' custom_font='' custom_class='' admin_preview_bg='' av-desktop-hide='' av-medium-hide='' av-small-hide='' av-mini-hide='' av-medium-font-size-title='' av-small-font-size-title='' av-mini-font-size-title='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' av_uid='av-pieoi'][/av_heading]

                  [av_textblock size='' font_color='' color='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' av_uid='av-jocvn36l' custom_class='' admin_preview_bg='']
                      \"system_name\": \"Isah business software\",
                      \"system_version\": \"9\",
                  [/av_textblock]
              [/av_one_half]

              [av_one_half min_height='' vertical_alignment='' space='' custom_margin='' margin='0px' row_boxshadow='' row_boxshadow_color='' row_boxshadow_width='10' link='' linktarget='' link_hover='' padding='0px' highlight='' highlight_size='' border='' border_color='' radius='0px' column_boxshadow='' column_boxshadow_color='' column_boxshadow_width='10' background='bg_color' background_color='' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='' background_position='top left' background_repeat='no-repeat' animation='' mobile_breaking='' mobile_display='' av_uid='av-1mb6ea']

                  [av_image src='https://staging-erpportal.kinsta.cloud/wp-content/uploads/2018/11/Epicor-High-Resolution-Logo-300x105.jpg' attachment='254' attachment_size='medium' align='center' styling='' hover='' link='' target='' caption='' font_size='' appearance='' overlay_opacity='0.4' overlay_color='#000000' overlay_text_color='#ffffff' copyright='' animation='no-animation' av_uid='av-1ca8ki' custom_class='' admin_preview_bg=''][/av_image]

                  [av_textblock size='' font_color='' color='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' av_uid='av-jocvn36l' custom_class='' admin_preview_bg='']
                      \"system_logo\": \"https://erp-comp-staging.s3.amazonaws.com/media/uploads/company-image/dcaf6a0b-7c60-403c-8fdc-b545306ac17b/isah_logo.jpeg\",
                  [/av_textblock]
              [/av_one_half]

              [av_section min_height='' min_height_px='500px' padding='default' shadow='no-shadow' bottom_border='no-border-styling' bottom_border_diagonal_color='#333333' bottom_border_diagonal_direction='' bottom_border_style='' custom_margin='0px' custom_margin_sync='true' custom_arrow_bg='' id='Bedrijfsinformatie' color='main_color' background='bg_color' custom_bg='#f5f5f5' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='' attachment='' attachment_size='' attach='scroll' position='top left' repeat='no-repeat' video='' video_ratio='16:9' overlay_opacity='0.5' overlay_color='' overlay_pattern='' overlay_custom_pattern='' av_element_hidden_in_editor='0' av_uid='av-jocwb864' custom_class='']
              [av_three_fourth first min_height='' vertical_alignment='' space='' custom_margin='' margin='0px' row_boxshadow='' row_boxshadow_color='' row_boxshadow_width='10' link='' linktarget='' link_hover='' padding='0px' highlight='' highlight_size='' border='' border_color='' radius='0px' column_boxshadow='' column_boxshadow_color='' column_boxshadow_width='10' background='bg_color' background_color='' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='' background_position='top left' background_repeat='no-repeat' animation='' mobile_breaking='' mobile_display='' av_uid='av-mbptm']

              [av_image src='https://staging-erpportal.kinsta.cloud/wp-content/uploads/2018/11/Epicor-High-Resolution-Logo-300x105.jpg' attachment='254' attachment_size='medium' align='left' styling='' hover='' link='' target='' caption='' font_size='' appearance='' overlay_opacity='0.4' overlay_color='#000000' overlay_text_color='#ffffff' copyright='' animation='no-animation' av_uid='av-1ca8ki' custom_class='' admin_preview_bg=''][/av_image]

              [av_heading heading='Bedrijfs informatie' tag='h2' link_apply='' link='manually,http://' link_target='' style='blockquote modern-quote' size='' subheading_active='' subheading_size='15' margin='' padding='10' color='' custom_font='' custom_class='' admin_preview_bg='' av-desktop-hide='' av-medium-hide='' av-small-hide='' av-mini-hide='' av-medium-font-size-title='' av-small-font-size-title='' av-mini-font-size-title='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' av_uid='av-izu8i'][/av_heading]

              [av_textblock size='' font_color='' color='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' av_uid='av-jocvn36l' custom_class='' admin_preview_bg='']
              Epicor Software Corporation bietet branchenspezifische Unternehmenslösungen an, die auf die Anforderungen von Herstellungsbetrieben, Vertriebsunternehmen, Einzelhandel und Dienstleistungsunternehmen zugeschnitten sind. Egal ob in der Cloud, gehostet oder vor Ort – in jede Lösung fließen mehr als 45Jahre Erfahrung mit den individuellen Geschäftsprozessen und betrieblichen Anforderungen unserer Kunden ein.

              Heute verlassen sich weltweit über 20.000 Kunden in 150 Ländern auf unser Fachwissen und unsere Lösungen, um ihre Leistung und ihren Profit zu verbessern. Epicor steigert das Wachstum von Unternehmen auf der ganzen Welt mit Lösungen wie:
              <ul>
                  <li>Epicor Enterprise Resource Planning (ERP)</li>
                  <li>Finanzmanagement</li>
                  <li>Manufacturing Execution System (MES)</li>
                  <li>Lieferkettenmanagement (SCM)</li>
              </ul>
              Epicor ist ein weltweit führender ERP-Anbieter für mittelständische sowie große Fertigungs- und Handelsunternehmen. Epicors Produkte sind für den internationalen Einsatz konzipiert und liefern Unternehmen beeindruckende Vorteile. Epicor-Lösungen kurbeln nicht nur das Wachstum an, sondern managen auch komplexe Prozesse und steigern die Effizienz. Daraus ergeben sich leistungsstarke Lösungen, die Ressourcen für das Wachstum Ihres Unternehmens freisetzen. Bekannt für seine Innovationskraft und Branchenerfahrung übernimmt Epicor als zentraler Ansprechpartner die Verantwortung, die regionale und global agierende Unternehmen erfordern.Weitere Informationen finden Sie auf den Internetseiten von Epicor oder wenden Sie sich an uns. Aktuelle Neuigkeiten zu ERP finden Sie auf www.business-inspired-erp.de.
              <h2>Geschichte</h2>
              <strong>1984</strong> Gründung als Platinum Software Corporation
              <strong>1997</strong> Übernahme der Clientele Software Inc. (Clientele ist eine CRM-Lösung zur Abwicklung kundenorientierter Aufgaben. Die Anwendung setzt sich aus verschiedenen Modulen zusammen, unter anderem für den Einsatz in Service und Support, Vertrieb, Marketing sowie zur Einrichtung und Verwaltung von Self-Service-Portalen.
              <strong>1998</strong> Die Fusion zwischen der Platinum Software Corporation und der DataWorks Corporation resultiert in einem breiteren Angebot an ERP-Lösungen für den Einsatz in der verarbeitenden Industrie.
              <strong>2001</strong> Veräußerung der Platinum für Windows (PFW)-Anwendung an Best Software, einem von Sage Software übernommenen Unternehmen, unter dessen Name der Hersteller zunächst am US-amerikanischen Markt agiert.
              <strong>2002</strong> Aufkauf der Procurement- und Sourcing-Lösungen von Clarus.
              <strong>2004</strong> Die Übernahme des Unternehmens Scala Business Solutions erweitert die weltweite Präsenz des Herstellers auf nunmehr über 140 Länder.
              <strong>2005</strong> In Folge der Akquisition der CRS Retail Systems Inc. zählen zum Angebot für den Einsatz im Einzelhandel jetzt auch Point-of-Sale-Anwendungen.
              <strong>2008</strong> Epicor schließt die Akquisition von NSB Retail Systems ab.
              <strong>2010</strong> Epicor übernimmt Spectrum Human Resource Systems Corporation. Durch die Übernahme kommt eine leistungsfähige internationale End-to-End-Lösung für HCM mit flexiblen Lieferoptionen auf den Markt.
              <strong>2011</strong> Epicor Software Corporation wird mit der 1972 gegründeten Activant Solutions Inc. verschmolzen.
              <strong>2012</strong> Epicor Software Corporation übernimmt Solarsoft.
              <strong>2014</strong> Epicor Software Corporation übernimmt QuantiSense, Inc.
              <strong>2015</strong>, Epicor übernimmt die privat gehaltene ShopVisible, LLC.
              [/av_textblock]

              [av_textblock size='' font_color='' color='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' av_uid='av-jocvn36l' custom_class='' admin_preview_bg='']
              \"vendor_name\": \"Isah\",
              \"vendor_information\": \"Test vendor 1\"
              \"vendor_logo\": \"https://erp-comp-staging.s3.amazonaws.com/media/uploads/company-image/dcaf6a0b-7c60-403c-8fdc-b545306ac17b/isah_logo.jpeg\",
              [/av_textblock]

              [/av_three_fourth]
              [/av_section]

              [av_section min_height='' min_height_px='500px' padding='default' shadow='no-shadow' bottom_border='no-border-styling' bottom_border_diagonal_color='#333333' bottom_border_diagonal_direction='' bottom_border_style='' custom_margin='0px' custom_margin_sync='true' custom_arrow_bg='' id='ProductInfo' color='main_color' background='bg_color' custom_bg='' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='' attachment='' attachment_size='' attach='scroll' position='top left' repeat='no-repeat' video='' video_ratio='16:9' overlay_opacity='0.5' overlay_color='' overlay_pattern='' overlay_custom_pattern='' av_element_hidden_in_editor='0' av_uid='av-jocwerob' custom_class='']
              [av_three_fourth first min_height='' vertical_alignment='' space='' custom_margin='' margin='0px' row_boxshadow='' row_boxshadow_color='' row_boxshadow_width='10' link='' linktarget='' link_hover='' padding='0px' highlight='' highlight_size='' border='' border_color='' radius='0px' column_boxshadow='' column_boxshadow_color='' column_boxshadow_width='10' background='bg_color' background_color='' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='' background_position='top left' background_repeat='no-repeat' animation='' mobile_breaking='' mobile_display='' av_uid='av-144wdu']

              [av_heading heading='Product informatie' tag='h2' link_apply='' link='manually,http://' link_target='' style='blockquote modern-quote' size='' subheading_active='' subheading_size='15' margin='' margin_sync='true' padding='10' color='' custom_font='' av-medium-font-size-title='' av-small-font-size-title='' av-mini-font-size-title='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' av_uid='av-joeh7gn8' custom_class='' admin_preview_bg=''][/av_heading]

              [av_textblock size='' font_color='' color='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' av_uid='av-jocvn36l' custom_class='' admin_preview_bg='']
              <h3>Beschreibung</h3>
              Die IT-Werkzeuge zur Steuerung von Unternehmen haben sich in den letzten Jahren grundlegend verändert. Heute muss ein Unternehmen überall und rund um die Uhr präsent sein, immer in Echtzeit, immer online. Das Internet und eine schier grenzenlose Mobilität spielen im Alltag unserer informationshungrigen globalen Wirtschaft eine zentrale Rolle. Die Welt verändert sich und mit Epicor hat sich auch die Welt der ERP-Lösungen verändert.

              Epicor-Anwendungen der nächsten Generation bieten Unternehmen die Möglichkeit, sich komplett neu aufzustellen. Epicor wurde für Menschen und Unternehmen von heute gemacht und ist dennoch immer auf das Morgen vorbereitet. Epicor erlaubt „Business without Barriers“.

              Technologische Barrieren, Integrationsbarrieren, Zugangsbarrieren – alles, was die Produktivität behindert, ist nun einfach weggeräumt. Früher nur eingeschränkt möglich, gibt es heute ungeahnte Möglichkeiten für eine transparente, teamübergreifende Zusammenarbeit und effiziente Analyseoptionen. Epicor True SOA™, unsere hoch konfigurierbare globale ERP-Plattform, bietet eine Funktionsbreite, die das Konzept von Unternehmenssoftware neu definiert.

              Die Epicor-Technologie bietet völlig neue Möglichkeit für das Management von Unternehmen und unterstützt dauerhafte Performance durch Echtzeit- und kontextgebundene betriebswirtschaftliche Erkenntnisse. Herz und Hirn von Epicor ist eine flexible und kollaborative Architektur, die die Bedürfnisse jedes Unternehmens abdeckt und länder-, branchen- und geräteunabhängige unternehmerische Arbeit ermöglicht.

              Epicor Lösungen:
              <ul>
                  <li>Finanzmanagement</li>
                  <li>Customer Relationship Management</li>
                  <li>Vertriebssteuerung</li>
                  <li>Supply Chain Management</li>
                  <li>Produktionsmanagement</li>
                  <li>Planung und Disposition</li>
                  <li>Produktdatenmanagement</li>
                  <li>Servicemanagement</li>
                  <li>Enterprise Performance Management</li>
                  <li>Globales Unternehmensmanagement</li>
                  <li>Governance, Risk und Compliance</li>
                  <li>Epicor Geschäftsarchitektur</li>
              </ul>
              Zielgruppe
              <ul>
                  <li>Maschinen- und Anlagebau</li>
                  <li>Metallverarbeitung</li>
                  <li>Automobilindustrie</li>
                  <li>Kunststoffverarbeitung</li>
                  <li>Medizintechnik</li>
                  <li>Elektrotechnik und Elektronik</li>
              </ul>
              <h3>Automatisierung</h3>
              Epicor unterstützt Unternehmen bei der Automatisierung durch Workflows und Anbindung anderer Software-Komponenten mittels seiner offenen Architektur und einfachen Erweiterbarkeit. Epicor verbindet Buchhaltung, Vertrieb, Produktion und andere Abteilungen des Unternehmensund schafft nahtlose Prozesse, die mittels Workflows automatisiert werden können.
              <h3>Besonderheiten</h3>
              Epicor ERP ist eine Unternehmenslösung für mittelständische Unternehmen und Geschäftsbereiche globaler Konzerne der Fertigungsindustrie und des Handels. Die SOA-Architektur auf .NET-Technologie ermöglicht Unternehmen eine zeitnahe und kostengünstige Anpassung der Prozesse an sich schnell ändernde Marktbedingungen. Web 2.0-Funktionen erlauben den Zugriff auf Firmendaten für jeden Mitarbeiter.

              Epicor ERP legt einen Schwerpunkt auf den Zugriff der Daten durch den Anwender. Es stehen moderne Werkzeuge zur Datenanalyse (BI) und für ein flexibles Berichtswesen zur Verfügung. Die Anbindung von mobilen Geräten für den Außendienst (Mobile Field Service) ist ein weiterer Schwerpunkt. Durch rollenbasierte Analysefunktionen, operative Berichte und Dashboards – auch auf Mobilgeräten – erhöht sich die Produktivität der Mitarbeiter. Die Integration der ERP-Funktionalitäten in MS Office reduziert den Trainingsaufwand, verbessert die Akzeptanz und beschleunigt die Entscheidungsfindung. Web 2.0-Funktionalitäten wie elektronische Teamarbeit oder Enterprise Search unterstützen den Anwender direkt mit für ihn notwendigen Informationen.

              Die SOA-Architektur erlaubt eine schnelle und flexible Implementierung, wie auch spätere Anpassungen von Abläufen bei veränderten Marktbedingungen. Industrieunternehmen können damit nachweislich ihre Gesamtbetriebskosten reduzieren, erzielen gleichzeitig eine schnellere Kapitalrendite und investieren in ein zukunftstaugliches Produkt.

              Epicor ERP macht Industrieunternehmen mit einer flexiblen, zukunftsweisenden Technologie in Verbindung mit geringen Gesamtbetriebskosten und einer über 45-jährigen Erfahrung mit Unternehmenslösungen für die mittelständische Fertigung fit für die Zukunft. Die multinationale Ausrichtung von Epicor auf Produkte wie Unternehmen liefert eine Antwort auf die Fragen der Digitalisierung in der heutigen globalen Wirtschaft.
              <h3>In Planung</h3>
              Die aktuell verfügbaren Funktionen insbesondere im Bereich „Projektmanagement“ machen Epicor ERP auch für professionelle Dienstleistungsunternehmen interessant. Hierbei können gerade diejenigen Dienstleister profitieren, die als „Zeitverkäufer“ mindestens 75% ihrer Umsätze mit dem Anbieten von Kompetenz, Expertise und Beratung erzielen. Hierzu zählen beispielsweise Beratungsunternehmen und Finanzdienstleister.In etwa 70% der internationalen Hotelketten werden bereits Epicor-Produkte eingesetzt.
              <h3>Langfristige Entwicklung</h3>
              Epicor ERP als SaaS-Lösung könnte die ERP-Kosten für Industrieunternehmen spürbar senken, da nur die tatsächlich benötigten Funktionen berechnet würden. Die Verlagerung der Rechenleistung in die Cloud kann weitere Möglichkeiten der Kostensenkung liefern. Für geschäftsrelevante Entscheidungen soll das Mobilitätskonzept auf weitere Geschäftsbereiche wie Management und Produktion ausgeweitet werden.
              <h3>Preisgestaltung</h3>
              Auf Anfrage
              [/av_textblock]

              [av_textblock size='' font_color='' color='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' av_uid='av-jocvn36l' custom_class='' admin_preview_bg='']
              \"system_name\": \"Isah business software\",
              \"system_version\": \"9\",
              \"system_description\": \"Isah business software\"
              \"system_focus\": \"Production\"
              [/av_textblock]

              [/av_three_fourth]
              [/av_section]

              ");


              $screenshots = get_post_meta( get_the_ID(), 'screenshots', true );

              if(!empty($screenshots) && is_array($screenshots)){

                      $screenshots_ids_string = '';
                      foreach ($screenshots as $screenshot) {
                          $screenshots_ids_string .= $screenshot['media_id'].',';
                      }

                      echo do_shortcode("
                      [av_section min_height='' min_height_px='500px' padding='default' shadow='no-shadow' bottom_border='no-border-styling' bottom_border_diagonal_color='#333333' bottom_border_diagonal_direction='' bottom_border_style='' custom_margin='0px' custom_margin_sync='true' custom_arrow_bg='' id='' color='main_color' background='bg_color' custom_bg='#f5f5f5' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='' attachment='' attachment_size='' attach='scroll' position='top left' repeat='no-repeat' video='' video_ratio='16:9' overlay_opacity='0.5' overlay_color='' overlay_pattern='' overlay_custom_pattern='' av_element_hidden_in_editor='0' av_uid='av-jocwtop7' custom_class='']
                          [av_heading heading='Product screenshots' tag='h2' link_apply='' link='manually,http://' link_target='' style='blockquote modern-quote' size='' subheading_active='' subheading_size='15' margin='' padding='10' color='' custom_font='' custom_class='' admin_preview_bg='' av-desktop-hide='' av-medium-hide='' av-small-hide='' av-mini-hide='' av-medium-font-size-title='' av-small-font-size-title='' av-mini-font-size-title='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' av_uid='av-a1b4i'][/av_heading]
                          [av_horizontal_gallery ids='".$screenshots_ids_string."' height='25' size='large' links='active' lightbox_text='' link_dest='' gap='large' active='enlarge' initial='' control_layout='av-control-default' id='' av_uid='av-jocw3iwm' custom_class=''][/av_horizontal_gallery]
                      [/av_section]
                      ");
              }

              $uuid = get_post_meta( get_the_ID(), 'uuid', true );

              $vendor_news_items_string = '';
              $vendor_news = get_posts(array(
                    'post_type' => 'vendor-news',
                    'posts_per_page' => -1,
                    'tax_query' => array(
                      array(
                        'taxonomy' => 'rffw_vendors_category',
                        'field' => 'slug',
                        'terms' => $uuid
                      )
                    )
                  ));
                  if(!empty($vendor_news) && is_array($vendor_news)){
                      foreach ($vendor_news as $vendor_news_item) {
                          $vendor_news_items_string .= "<li><a title=\"".$vendor_news_item->post_title."\" href=\"".get_the_permalink($vendor_news_item->ID)."\">".$vendor_news_item->post_title."</a></li>";
                      }
                  }



              echo do_shortcode("[av_section min_height='' min_height_px='500px' padding='default' shadow='no-shadow' bottom_border='no-border-styling' bottom_border_diagonal_color='#333333' bottom_border_diagonal_direction='' bottom_border_style='' custom_margin='0px' custom_margin_sync='true' custom_arrow_bg='' id='' color='main_color' background='bg_color' custom_bg='' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='' attachment='' attachment_size='' attach='scroll' position='top left' repeat='no-repeat' video='' video_ratio='16:9' overlay_opacity='0.5' overlay_color='' overlay_pattern='' overlay_custom_pattern='' av_element_hidden_in_editor='0' av_uid='av-jocwerob' custom_class='']
              [av_three_fourth first min_height='' vertical_alignment='' space='' custom_margin='' margin='0px' row_boxshadow='' row_boxshadow_color='' row_boxshadow_width='10' link='' linktarget='' link_hover='' padding='0px' highlight='' highlight_size='' border='' border_color='' radius='0px' column_boxshadow='' column_boxshadow_color='' column_boxshadow_width='10' background='bg_color' background_color='' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='' background_position='top left' background_repeat='no-repeat' animation='' mobile_breaking='' mobile_display='' av_uid='av-qgwyq']

              [av_heading heading='Nieuws' tag='h2' link_apply='' link='manually,http://' link_target='' style='blockquote modern-quote' size='' subheading_active='' subheading_size='15' margin='' margin_sync='true' padding='10' color='' custom_font='' av-medium-font-size-title='' av-small-font-size-title='' av-mini-font-size-title='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' av_uid='av-joegtyap' custom_class='' admin_preview_bg=''][/av_heading]

              [av_textblock size='' font_color='' color='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' av_uid='av-jocvn36l' custom_class='' admin_preview_bg='']
              <div class=\"elementor-element elementor-element-48398e52 elementor-widget elementor-widget-wp-widget-listcategorypostswidget\" data-id=\"48398e52\" data-element_type=\"wp-widget-listcategorypostswidget.default\">
              <div class=\"elementor-widget-container\">
              <ul id=\"lcp_instance_listcategorypostswidget-REPLACE_TO_ID\" class=\"lcp_catlist\">".$vendor_news_items_string."</ul>
              </div>
              </div>
              [/av_textblock]

              [/av_three_fourth]
              [/av_section]");

              echo do_shortcode("[av_section min_height='' min_height_px='500px' padding='default' shadow='no-shadow' bottom_border='no-border-styling' bottom_border_diagonal_color='#333333' bottom_border_diagonal_direction='' bottom_border_style='' custom_margin='0px' custom_margin_sync='true' custom_arrow_bg='' id='' color='main_color' background='bg_color' custom_bg='#f5f5f5' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='' attachment='' attachment_size='' attach='scroll' position='top left' repeat='no-repeat' video='' video_ratio='16:9' overlay_opacity='0.5' overlay_color='' overlay_pattern='' overlay_custom_pattern='' av_element_hidden_in_editor='0' av_uid='av-jocwerob' custom_class='']
              [av_three_fourth first min_height='' vertical_alignment='' space='' custom_margin='' margin='0px' row_boxshadow='' row_boxshadow_color='' row_boxshadow_width='10' link='' linktarget='' link_hover='' padding='0px' highlight='' highlight_size='' border='' border_color='' radius='0px' column_boxshadow='' column_boxshadow_color='' column_boxshadow_width='10' background='bg_color' background_color='' background_gradient_color1='' background_gradient_color2='' background_gradient_direction='vertical' src='' background_position='top left' background_repeat='no-repeat' animation='' mobile_breaking='' mobile_display='' av_uid='av-b7ure']

              [av_heading heading='Contact' tag='h2' link_apply='' link='manually,http://' link_target='' style='blockquote modern-quote' size='' subheading_active='' subheading_size='15' margin='' margin_sync='true' padding='10' color='' custom_font='' av-medium-font-size-title='' av-small-font-size-title='' av-mini-font-size-title='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' av_uid='av-joegszo8' custom_class='' admin_preview_bg=''][/av_heading]

              [av_textblock size='' font_color='' color='' av-medium-font-size='' av-small-font-size='' av-mini-font-size='' av_uid='av-jocvn36l' custom_class='' admin_preview_bg='']
              <div class=\"elementor-element elementor-element-48398e52 elementor-widget elementor-widget-wp-widget-listcategorypostswidget\" data-id=\"48398e52\" data-element_type=\"wp-widget-listcategorypostswidget.default\">
              <div class=\"elementor-widget-container\"></div>
              </div>
              <div class=\"elementor-element elementor-element-13f2a1b8 elementor-widget elementor-widget-text-editor\" data-id=\"13f2a1b8\" data-element_type=\"text-editor.default\">
              <div class=\"elementor-widget-container\">
              <div class=\"elementor-text-editor elementor-clearfix\">
              <div class=\"elementor-element elementor-element-bb6f055 elementor-widget elementor-widget-heading\" data-id=\"bb6f055\" data-element_type=\"heading.default\">
              <div class=\"elementor-widget-container\"></div>
              </div>
              <div class=\"elementor-element elementor-element-3f943f60 elementor-widget elementor-widget-text-editor\" data-id=\"3f943f60\" data-element_type=\"text-editor.default\">
              <div class=\"elementor-widget-container\">
              <div class=\"elementor-text-editor elementor-clearfix\"><address>\"contact_info\": \"HTC 9-2, Eindhoven, NL, tel +31 40 755 5902\",
              \"phone_number\": \"123456\",
              \"website\": \"www.ridder.nl\",
              \"contact_person\": \"dummy contact person\",
              \"email\": \"dummy@ridder.com\",
              \"created_date\": \"2018-09-23\",
              \"updated_date\": \"2018-11-12\",</address></div>
              </div>
              </div>
              </div>
              </div>
              </div>
              [/av_textblock]

              [/av_three_fourth]
              [/av_section]
");
