<?php
global $avia_config, $post_loop_count;

$post_loop_count= 1;
$post_class 	= "post-entry-".avia_get_the_id();

// yay

// check if we got posts to display:
if (have_posts()) :

	while (have_posts()) : the_post();
?>

		<article class='post-entry post-entry-type-page <?php echo $post_class; ?>' <?php avia_markup_helper(array('context' => 'entry')); ?>>

			<div class="entry-content-wrapper clearfix">
                <?php
                /*echo '<header class="entry-content-header">';
                    $thumb = get_the_post_thumbnail(get_the_ID(), $avia_config['size']);

                    if($thumb) echo "<div class='page-thumb'>{$thumb}</div>";
                echo '</header>';*/

                //display the actual post content
                echo '<div class="entry-content" '.avia_markup_helper(array('context' => 'entry_content','echo'=>false)).'>';
                    //the_content(__('Read more','avia_framework').'<span class="more-link-arrow"></span>');

                    /* Run the loop to output the posts.
                    * If you want to overload this in a child theme then include a file
                    * called loop-page.php and that will be used instead.
                    */

                    //$avia_config['size'] = avia_layout_class( 'main' , false) == 'fullsize' ? 'entry_without_sidebar' : 'entry_with_sidebar';
                    //get_template_part( 'includes/loop', 'page' );



										$uuid = get_post_meta( get_the_ID(), 'uuid', true );

			              $vendor_news_items_string = '';
			              $vendor_news = get_posts(array(
			                    'post_type' => 'vendor-news',
			                    'posts_per_page' => -1,
			                    'tax_query' => array(
			                      array(
			                        'taxonomy' => 'rffw_vendors_category',
			                        'field' => 'slug',
			                        'terms' => $uuid
			                      )
			                    )
			                  ));

	                  if(!empty($vendor_news) && is_array($vendor_news)){
	                      foreach ($vendor_news as $vendor_news_item) {
	                          $vendor_news_items_string .= "<li><a title=\"".$vendor_news_item->post_title."\" href=\"".get_the_permalink($vendor_news_item->ID)."\">".$vendor_news_item->post_title."</a></li>";
	                      }
	                  }



										$screenshots = get_post_meta( get_the_ID(), 'screenshots', true );
										$screenshots_ids_string = '';
			              if(!empty($screenshots) && is_array($screenshots)){

                      foreach ($screenshots as $screenshot) {
                          $screenshots_ids_string .= $screenshot['media_id'].',';
                      }
			              }

										//echo  do_shortcode("[av_submenu which_menu='custom' menu='3' position='left' color='main_color' sticky='aviaTBsticky' mobile='disabled' av_uid='av-18rv6q' custom_class='']
										              //     [av_submenu_item title='Bedrijfs informatie' link='manually, #Bedrijfsinformatie' linktarget='' button_style='av-menu-button av-menu-button-bordered' av_uid='av-78kky']
										              //     [av_submenu_item title='Product Info' link='manually, #ProductInfo' linktarget='' button_style='' av_uid='av-oonoy']
										              //     [av_submenu_item title='Product screenshots' link='manually,#ProductInfo' linktarget='' button_style='' av_uid='av-j7vl6']
										              //     [av_submenu_item title='Nieuws' link='manually,#ProductInfo' linktarget='' button_style='' av_uid='av-1ayrm']
										              //     [av_submenu_item title='Contact' link='manually,#Contact' linktarget='' button_style='' av_uid='av-aakgi']
										              // [/av_submenu]");

                    ?>


				    	<div class="clear"></div>
				    	<div id='sub_menu1' class='av-submenu-container main_color   av-sticky-submenu container_wrap fullsize' style='  z-index:301'>
				      <div class='container av-menu-mobile-disabled '>
				        <ul id='av-custom-submenu-1' class='av-subnav-menu av-submenu-pos-left'>
				          <li class='menu-item menu-item-top-level av-menu-button av-menu-button-bordered menu-item-top-level-1'><a href=' #Bedrijfsinformatie'><span class='avia-bullet'></span><span class='avia-menu-text'>Bedrijfs informatie</span></a></li>
				          <li class='menu-item menu-item-top-level  menu-item-top-level-2'><a href=' #ProductInfo'><span class='avia-bullet'></span><span class='avia-menu-text'>Product Info</span></a></li>
									<?php if(!empty($screenshots_ids_string)){ ?>
					          <li class='menu-item menu-item-top-level  menu-item-top-level-3'><a href='#Screenshots'><span class='avia-bullet'></span><span class='avia-menu-text'>Product screenshots</span></a></li>
									<?php } ?>
									<?php if(!empty($vendor_news_items_string)){ ?>
				          	<li class='menu-item menu-item-top-level  menu-item-top-level-4'><a href='#Nieuws'><span class='avia-bullet'></span><span class='avia-menu-text'>Nieuws</span></a></li>
									<?php } ?>
				          <li class='menu-item menu-item-top-level  menu-item-top-level-5'><a href='#Contact'><span class='avia-bullet'></span><span class='avia-menu-text'>Contact</span></a></li>
				        </ul>
				      </div>
				    </div>
				    <div class='sticky_placeholder'></div>

						<div class='container'>
				    <div class="flex_column av_one_half  flex_column_div av-zero-column-padding first  " style='border-radius:0px; '>
				      <div style='padding-bottom:10px; ' class='av-special-heading av-special-heading-h1  blockquote modern-quote   '>
				        <h1 class='av-special-heading-tag ' itemprop="headline"><?php echo get_the_title(); ?></h1>
				        <div class='special-heading-border'>
				          <div class='special-heading-inner-border'></div>
				        </div>
				      </div>
				      <section class="av_textblock_section " itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
				        <div class='avia_textblock  ' itemprop="text">
										<p>
											Systeem: <?php echo get_post_meta( get_the_ID(), 'system_name', true ); ?></br>
											<!-- system version: <?php echo get_post_meta( get_the_ID(), 'system_version', true ); ?> -->
										</p>
				        </div>
				      </section>
				    </div>
				    <div class="flex_column av_one_half  flex_column_div av-zero-column-padding   " style='border-radius:0px; '>
				      <div class='avia-image-container  av-styling-     avia-align-center ' itemprop="ImageObject" itemscope="itemscope" itemtype="https://schema.org/ImageObject">
				        <div class='avia-image-container-inner'>
				          <div class='avia-image-overlay-wrap' style="margin: 20px 0 0 0;">
										<?php echo wp_get_attachment_image(get_post_meta( get_the_ID(), 'system_logo_id', true ), 'medium_large', "", array( "style" => "max-width:300px;height:auto;" )); ?>
									</div>
				        </div>
				      </div>
				    </div>
						</div>

				  <div id='Bedrijfsinformatie' class='avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll    container_wrap fullsize' style='background-color: #f5f5f5;  '>
				    <div class='container'>
				      <div class='template-page content  av-content-full alpha units'>
				        <div class='post-entry post-entry-type-page post-entry-1410'>
				          <div class='entry-content-wrapper clearfix'>
				            <div class="flex_column av_three_fourth  flex_column_div av-zero-column-padding first  " style='border-radius:0px; '>
				              <div class='avia-image-container  av-styling-     avia-align-left ' itemprop="ImageObject" itemscope="itemscope" itemtype="https://schema.org/ImageObject">
				                <div class='avia-image-container-inner'>
				                  <div class='avia-image-overlay-wrap' style="margin: 15px 0 10px 0;">
															<?php echo wp_get_attachment_image(get_post_meta( get_the_ID(), 'vendor_logo_id', true ), 'medium_large', "", array( "style" => "max-width:300px;height:auto;" )); ?>
													</div>
				                </div>
				              </div>
				              <div style='padding-bottom:10px; ' class='av-special-heading av-special-heading-h2  blockquote modern-quote   '>
				                <h2 class='av-special-heading-tag ' itemprop="headline">Bedrijfs informatie</h2>
				                <div class='special-heading-border'>
				                  <div class='special-heading-inner-border'></div>
				                </div>
				              </div>
				              <section class="av_textblock_section " itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
				                <div class='avia_textblock  ' itemprop="text">


				                </div>
				              </section>
				              <section class="av_textblock_section " itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
				                <div class='avia_textblock  ' itemprop="text">
													<h3><?php echo get_the_title(); ?></h3>
													<p>
														<?php //echo get_post_meta( get_the_ID(), 'vendor_name', true ); ?>
														<?php echo apply_filters('the_content',get_post_meta( get_the_ID(), 'vendor_information', true )); ?>
													</p>
													<?php echo get_the_post_thumbnail(get_post_meta( get_the_ID(), 'vendor_logo_id', true ), 'medium_large', "", array( "style" => "max-width:300px;height:auto;" )); ?>
				                </div>
				              </section>
				            </div>


				          </div>
				        </div>
				      </div><!-- close content main div -->
				    </div>
				  </div>

				  <div id='ProductInfo' class='avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll    container_wrap fullsize' style=' '>
				    <div class='container'>
				      <div class='template-page content  av-content-full alpha units'>
				        <div class='post-entry post-entry-type-page post-entry-1410'>
				          <div class='entry-content-wrapper clearfix'>
				            <div class="flex_column av_three_fourth  flex_column_div av-zero-column-padding first  " style='border-radius:0px; '>
				              <div style='padding-bottom:10px; ' class='av-special-heading av-special-heading-h2  blockquote modern-quote   '>
				                <h2 class='av-special-heading-tag ' itemprop="headline">Product informatie</h2>
				                <div class='special-heading-border'>
				                  <div class='special-heading-inner-border'></div>
				                </div>
				              </div>
				              <section class="av_textblock_section " itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
				                <div class='avia_textblock  ' itemprop="text">



				                </div>
				              </section>
				              <section class="av_textblock_section " itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
				                <div class='avia_textblock  ' itemprop="text">
				                  						<h3><?php echo get_post_meta( get_the_ID(), 'system_name', true ); ?> <?php echo get_post_meta( get_the_ID(), 'system_version', true ); ?></h3>
				                  						<p>
															<?php echo apply_filters('the_content',get_post_meta( get_the_ID(), 'system_description', true )); ?></br>
															Systeem focus: <?php echo get_post_meta( get_the_ID(), 'system_focus', true ); ?></br>
														</p>
														<?php echo get_the_post_thumbnail(get_post_meta( get_the_ID(), 'system_logo_id', true ), 'medium_large', "", array( "style" => "max-width:300px;height:auto;" )); ?>
				                </div>
				              </section>
				            </div>


				          </div>
				        </div>
				      </div><!-- close content main div -->
				    </div>
				  </div>


<?php if(!empty($screenshots_ids_string)){ ?>

<div id="Screenshots" class="avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll container_wrap fullsize" style="background-color: #f5f5f5;  ">
	<div class="container">
		<div class="template-page content  av-content-full alpha units">
		<div class="post-entry post-entry-type-page post-entry-699">
			<div class="entry-content-wrapper clearfix">
          <div style="padding-bottom:10px; " class="av-special-heading av-special-heading-h2  blockquote modern-quote   ">
						<h2 class="av-special-heading-tag " itemprop="headline">Product screenshots</h2>
						<div class="special-heading-border">
							<div class="special-heading-inner-border"></div>
						</div>
					</div>

        	<?php echo do_shortcode("[av_horizontal_gallery ids='".$screenshots_ids_string."' height='25' size='large' links='active' lightbox_text='' link_dest='' gap='large' active='enlarge' initial='' control_layout='av-control-default' id='' av_uid='av-jocw3iwm' custom_class=''][/av_horizontal_gallery]"); ?>

        </div>
			</div>
		</div><!-- close content main div -->
	</div>
</div>
<?php } ?>

<?php if(!empty($vendor_news_items_string)){ ?>

				  <div id='Nieuws' class='avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll    container_wrap fullsize' style=' '>
				    <div class='container'>
				      <div class='template-page content  av-content-full alpha units'>
				        <div class='post-entry post-entry-type-page post-entry-1410'>
				          <div class='entry-content-wrapper clearfix'>
				            <div class="flex_column av_three_fourth  flex_column_div av-zero-column-padding first  " style='border-radius:0px; '>
				              <div style='padding-bottom:10px; ' class='av-special-heading av-special-heading-h2  blockquote modern-quote   '>
				                <h2 class='av-special-heading-tag ' itemprop="headline">Nieuws</h2>
				                <div class='special-heading-border'>
				                  <div class='special-heading-inner-border'></div>
				                </div>
				              </div>
				              <section class="av_textblock_section " itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
				                <div class='avia_textblock  ' itemprop="text">
				                  <div class="elementor-element elementor-element-48398e52 elementor-widget elementor-widget-wp-widget-listcategorypostswidget" data-id="48398e52" data-element_type="wp-widget-listcategorypostswidget.default">
				                    <div class="elementor-widget-container">
				                      <ul id="lcp_instance_listcategorypostswidget-REPLACE_TO_ID" class="lcp_catlist">
																<?php echo $vendor_news_items_string; ?>
															</ul>
				                      </p>
				                    </div>
				                  </div>
				                  <p>
				                </div>
				              </section>
				            </div>
				          </div>
				        </div>
				      </div><!-- close content main div -->
				    </div>
				  </div>
	<?php } ?>

				  <div id='Contact' class='avia-section main_color avia-section-default avia-no-shadow avia-bg-style-scroll    container_wrap fullsize' style='background-color: #f5f5f5;  '>
				    <div class='container'>
				      <div class='template-page content  av-content-full alpha units'>
				        <div class='post-entry post-entry-type-page post-entry-1410'>
				          <div class='entry-content-wrapper clearfix'>
				            <div class="flex_column av_three_fourth  flex_column_div av-zero-column-padding first  " style='border-radius:0px; '>
				              <div style='padding-bottom:10px; ' class='av-special-heading av-special-heading-h2  blockquote modern-quote   '>
				                <h2 class='av-special-heading-tag ' itemprop="headline">Contact</h2>
				                <div class='special-heading-border'>
				                  <div class='special-heading-inner-border'></div>
				                </div>
				              </div>
				              <section class="av_textblock_section " itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
				                <div class='avia_textblock  ' itemprop="text">
				                  <div class="elementor-element elementor-element-48398e52 elementor-widget elementor-widget-wp-widget-listcategorypostswidget" data-id="48398e52" data-element_type="wp-widget-listcategorypostswidget.default">
				                    <div class="elementor-widget-container"></div>
				                    </p>
				                  </div>
				                  <div class="elementor-element elementor-element-13f2a1b8 elementor-widget elementor-widget-text-editor" data-id="13f2a1b8" data-element_type="text-editor.default">
				                    <div class="elementor-widget-container">
				                      <div class="elementor-text-editor elementor-clearfix">
				                        <div class="elementor-element elementor-element-bb6f055 elementor-widget elementor-widget-heading" data-id="bb6f055" data-element_type="heading.default">
				                          <div class="elementor-widget-container"></div>
				                          </p>
				                        </div>
				                        <div class="elementor-element elementor-element-3f943f60 elementor-widget elementor-widget-text-editor" data-id="3f943f60" data-element_type="text-editor.default">
				                          <div class="elementor-widget-container">
				                            <div class="elementor-text-editor elementor-clearfix">
				                              <address>
																				<?php echo get_post_meta( get_the_ID(), 'contact_info', true ); ?></br>
																				<?php echo get_post_meta( get_the_ID(), 'phone_number', true ); ?></br>
				                                <?php echo get_post_meta( get_the_ID(), 'website', true ); ?></br>
				                                <?php echo get_post_meta( get_the_ID(), 'contact_person', true ); ?></br>
																				<?php echo get_post_meta( get_the_ID(), 'email', true ); ?></br>
																				<!-- <?php echo get_post_meta( get_the_ID(), 'created_date', true ); ?></br>
																				<?php echo get_post_meta( get_the_ID(), 'updated_date', true ); ?></br> -->


																			</address>
				                            </div>
				                          </div>
				                        </div>
				                      </div>
				                    </div>
				                  </div>
				                  <p>
				                </div>
				              </section>
				            </div>
									</div>
						    </div>
						  </div>
						  </div>
						  </div>

<?php




                echo '</div>';

                echo '<footer class="entry-footer">';
                wp_link_pages(array('before' =>'<div class="pagination_split_post">',
                                        'after'  =>'</div>',
                                        'pagelink' => '<span>%</span>'
                                        ));
                echo '</footer>';

                do_action('ava_after_content', get_the_ID(), 'page');
                ?>
			</div>

		</article><!--end post-entry-->


<?php
	$post_loop_count++;
	endwhile;
	else:
?>

    <article class="entry">
        <header class="entry-content-header">
            <h1 class='post-title entry-title'><?php _e('Nothing Found', 'avia_framework'); ?></h1>
        </header>

        <?php get_template_part('includes/error404'); ?>

        <footer class="entry-footer"></footer>
    </article>

<?php

	endif;
?>
